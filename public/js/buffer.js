var fbjcerrors = [{error: ''}]; /*! Sizzle v1.10.12-pre | (c) 2013 jQuery Foundation, Inc. | jquery.org/license
//@ sourceMappingURL=sizzle.min.map
*/
!function(a) {
   function b(a, b, c, d) {
       var e, f, g, h, i, j, l, o, p, q;
       if ((b ? b.ownerDocument || b : P) !== H && G(b), b = b || H, c = c || [], !a || "string" != typeof a)
           return c;
       if (1 !== (h = b.nodeType) && 9 !== h)
           return [];
       if (J && !d) {
           if (e = tb.exec(a))
               if (g = e[1]) {
                   if (9 === h) {
                       if (f = b.getElementById(g), !f || !f.parentNode)
                           return c;
                       if (f.id === g)
                           return c.push(f), c
                   } else if (b.ownerDocument && (f = b.ownerDocument.getElementById(g)) && N(b, f) && f.id === g)
                       return c.push(f), c
               } else {
                   if (e[2])
                       return ab.apply(c, b.getElementsByTagName(a)), c;
                   if ((g = e[3]) && x.getElementsByClassName && b.getElementsByClassName)
                       return ab.apply(c, b.getElementsByClassName(g)), c
               }
           if (x.qsa && (!K || !K.test(a))) {
               if (o = l = O, p = b, q = 9 === h && a, 1 === h && "object" !== b.nodeName.toLowerCase()) {
                   for (j = m(a), (l = b.getAttribute("id")) ? o = l.replace(vb, "\\$&") : b.setAttribute("id", o), o = "[id='" + o + "'] ", i = j.length; i--; )
                       j[i] = o + n(j[i]);
                   p = ub.test(a) && k(b.parentNode) || b, q = j.join(",")
               }
               if (q)
                   try {
                       return ab.apply(c, p.querySelectorAll(q)), c
                   } catch (r) {
                   }finally {
                       l || b.removeAttribute("id")
                   }
           }
       }
       return v(a.replace(jb, "$1"), b, c, d)
   }
   function c() {
       function a(c, d) {
           return b.push(c + " ") > z.cacheLength && delete a[b.shift()], a[c + " "] = d
       }
       var b = [];
       return a
   }
   function d(a) {
       return a[O] = !0, a
   }
   function e(a) {
       var b = H.createElement("div");
       try {
           return !!a(b)
       } catch (c) {
           return !1
       }finally {
           b.parentNode && b.parentNode.removeChild(b), b = null
       }
   }
   function f(a, b) {
       for (var c = a.split("|"), d = a.length; d--; )
           z.attrHandle[c[d]] = b
   }
   function g(a, b) {
       var c = b && a, d = c && 1 === a.nodeType && 1 === b.nodeType && (~b.sourceIndex || X) - (~a.sourceIndex || X);
       if (d)
           return d;
       if (c)
           for (; c = c.nextSibling; )
               if (c === b)
                   return -1;
       return a ? 1 : -1
   }
   function h(a) {
       return function(b) {
           var c = b.nodeName.toLowerCase();
           return "input" === c && b.type === a
       }
   }
   function i(a) {
       return function(b) {
           var c = b.nodeName.toLowerCase();
           return ("input" === c || "button" === c) && b.type === a
       }
   }
   function j(a) {
       return d(function(b) {
           return b = +b, d(function(c, d) {
               for (var e, f = a([], c.length, b), g = f.length; g--; )
                   c[e = f[g]] && (c[e] = !(d[e] = c[e]))
           })
       })
   }
   function k(a) {
       return a && typeof a.getElementsByTagName !== W && a
   }
   function l() {
   }
   function m(a, c) {
       var d, e, f, g, h, i, j, k = T[a + " "];
       if (k)
           return c ? 0 : k.slice(0);
       for (h = a, i = [], j = z.preFilter; h; ) {
           (!d || (e = kb.exec(h))) && (e && (h = h.slice(e[0].length) || h), i.push(f = [])), d = !1, (e = lb.exec(h)) && (d = e.shift(), f.push({value: d,type: e[0].replace(jb, " ")}), h = h.slice(d.length));
           for (g in z.filter)
               !(e = pb[g].exec(h)) || j[g] && !(e = j[g](e)) || (d = e.shift(), f.push({value: d,type: g,matches: e}), h = h.slice(d.length));
           if (!d)
               break
       }
       return c ? h.length : h ? b.error(a) : T(a, i).slice(0)
   }
   function n(a) {
       for (var b = 0, c = a.length, d = ""; c > b; b++)
           d += a[b].value;
       return d
   }
   function o(a, b, c) {
       var d = b.dir, e = c && "parentNode" === d, f = R++;
       return b.first ? function(b, c, f) {
           for (; b = b[d]; )
               if (1 === b.nodeType || e)
                   return a(b, c, f)
       } : function(b, c, g) {
           var h, i, j, k = Q + " " + f;
           if (g) {
               for (; b = b[d]; )
                   if ((1 === b.nodeType || e) && a(b, c, g))
                       return !0
           } else
               for (; b = b[d]; )
                   if (1 === b.nodeType || e)
                       if (j = b[O] || (b[O] = {}), (i = j[d]) && i[0] === k) {
                           if ((h = i[1]) === !0 || h === y)
                               return h === !0
                       } else if (i = j[d] = [k], i[1] = a(b, c, g) || y, i[1] === !0)
                           return !0
       }
   }
   function p(a) {
       return a.length > 1 ? function(b, c, d) {
           for (var e = a.length; e--; )
               if (!a[e](b, c, d))
                   return !1;
           return !0
       } : a[0]
   }
   function q(a, b, c, d, e) {
       for (var f, g = [], h = 0, i = a.length, j = null != b; i > h; h++)
           (f = a[h]) && (!c || c(f, d, e)) && (g.push(f), j && b.push(h));
       return g
   }
   function r(a, b, c, e, f, g) {
       return e && !e[O] && (e = r(e)), f && !f[O] && (f = r(f, g)), d(function(d, g, h, i) {
           var j, k, l, m = [], n = [], o = g.length, p = d || u(b || "*", h.nodeType ? [h] : h, []), r = !a || !d && b ? p : q(p, m, a, h, i), s = c ? f || (d ? a : o || e) ? [] : g : r;
           if (c && c(r, s, h, i), e)
               for (j = q(s, n), e(j, [], h, i), k = j.length; k--; )
                   (l = j[k]) && (s[n[k]] = !(r[n[k]] = l));
           if (d) {
               if (f || a) {
                   if (f) {
                       for (j = [], k = s.length; k--; )
                           (l = s[k]) && j.push(r[k] = l);
                       f(null, s = [], j, i)
                   }
                   for (k = s.length; k--; )
                       (l = s[k]) && (j = f ? cb.call(d, l) : m[k]) > -1 && (d[j] = !(g[j] = l))
               }
           } else
               s = q(s === g ? s.splice(o, s.length) : s), f ? f(null, g, s, i) : ab.apply(g, s)
       })
   }
   function s(a) {
       for (var b, c, d, e = a.length, f = z.relative[a[0].type], g = f || z.relative[" "], h = f ? 1 : 0, i = o(function(a) {
           return a === b
       }, g, !0), j = o(function(a) {
           return cb.call(b, a) > -1
       }, g, !0), k = [function(a, c, d) {
               return !f && (d || c !== D) || ((b = c).nodeType ? i(a, c, d) : j(a, c, d))
           }]; e > h; h++)
           if (c = z.relative[a[h].type])
               k = [o(p(k), c)];
           else {
               if (c = z.filter[a[h].type].apply(null, a[h].matches), c[O]) {
                   for (d = ++h; e > d && !z.relative[a[d].type]; d++)
                       ;
                   return r(h > 1 && p(k), h > 1 && n(a.slice(0, h - 1).concat({value: " " === a[h - 2].type ? "*" : ""})).replace(jb, "$1"), c, d > h && s(a.slice(h, d)), e > d && s(a = a.slice(d)), e > d && n(a))
               }
               k.push(c)
           }
       return p(k)
   }
   function t(a, c) {
       var e = 0, f = c.length > 0, g = a.length > 0, h = function(d, h, i, j, k) {
           var l, m, n, o = 0, p = "0", r = d && [], s = [], t = D, u = d || g && z.find.TAG("*", k), v = Q += null == t ? 1 : Math.random() || .1, w = u.length;
           for (k && (D = h !== H && h, y = e); p !== w && null != (l = u[p]); p++) {
               if (g && l) {
                   for (m = 0; n = a[m++]; )
                       if (n(l, h, i)) {
                           j.push(l);
                           break
                       }
                   k && (Q = v, y = ++e)
               }
               f && ((l = !n && l) && o--, d && r.push(l))
           }
           if (o += p, f && p !== o) {
               for (m = 0; n = c[m++]; )
                   n(r, s, h, i);
               if (d) {
                   if (o > 0)
                       for (; p--; )
                           r[p] || s[p] || (s[p] = $.call(j));
                   s = q(s)
               }
               ab.apply(j, s), k && !d && s.length > 0 && o + c.length > 1 && b.uniqueSort(j)
           }
           return k && (Q = v, D = t), r
       };
       return f ? d(h) : h
   }
   function u(a, c, d) {
       for (var e = 0, f = c.length; f > e; e++)
           b(a, c[e], d);
       return d
   }
   function v(a, b, c, d) {
       var e, f, g, h, i, j = m(a);
       if (!d && 1 === j.length) {
           if (f = j[0] = j[0].slice(0), f.length > 2 && "ID" === (g = f[0]).type && x.getById && 9 === b.nodeType && J && z.relative[f[1].type]) {
               if (b = (z.find.ID(g.matches[0].replace(wb, xb), b) || [])[0], !b)
                   return c;
               a = a.slice(f.shift().value.length)
           }
           for (e = pb.needsContext.test(a) ? 0 : f.length; e-- && (g = f[e], !z.relative[h = g.type]); )
               if ((i = z.find[h]) && (d = i(g.matches[0].replace(wb, xb), ub.test(f[0].type) && k(b.parentNode) || b))) {
                   if (f.splice(e, 1), a = d.length && n(f), !a)
                       return ab.apply(c, d), c;
                   break
               }
       }
       return C(a, j)(d, b, !J, c, ub.test(a) && k(b.parentNode) || b), c
   }
   var w, x, y, z, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O = "sizzle" + -new Date, P = a.document, Q = 0, R = 0, S = c(), T = c(), U = c(), V = function(a, b) {
       return a === b && (F = !0), 0
   }, W = typeof void 0, X = 1 << 31, Y = {}.hasOwnProperty, Z = [], $ = Z.pop, _ = Z.push, ab = Z.push, bb = Z.slice, cb = Z.indexOf || function(a) {
       for (var b = 0, c = this.length; c > b; b++)
           if (this[b] === a)
               return b;
       return -1
   }, db = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", eb = "[\\x20\\t\\r\\n\\f]", fb = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", gb = fb.replace("w", "w#"), hb = "\\[" + eb + "*(" + fb + ")" + eb + "*(?:([*^$|!~]?=)" + eb + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + gb + ")|)|)" + eb + "*\\]", ib = ":(" + fb + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + hb.replace(3, 8) + ")*)|.*)\\)|)", jb = new RegExp("^" + eb + "+|((?:^|[^\\\\])(?:\\\\.)*)" + eb + "+$", "g"), kb = new RegExp("^" + eb + "*," + eb + "*"), lb = new RegExp("^" + eb + "*([>+~]|" + eb + ")" + eb + "*"), mb = new RegExp("=" + eb + "*([^\\]'\"]*)" + eb + "*\\]", "g"), nb = new RegExp(ib), ob = new RegExp("^" + gb + "$"), pb = {ID: new RegExp("^#(" + fb + ")"),CLASS: new RegExp("^\\.(" + fb + ")"),TAG: new RegExp("^(" + fb.replace("w", "w*") + ")"),ATTR: new RegExp("^" + hb),PSEUDO: new RegExp("^" + ib),CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + eb + "*(even|odd|(([+-]|)(\\d*)n|)" + eb + "*(?:([+-]|)" + eb + "*(\\d+)|))" + eb + "*\\)|)", "i"),bool: new RegExp("^(?:" + db + ")$", "i"),needsContext: new RegExp("^" + eb + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + eb + "*((?:-\\d)?\\d*)" + eb + "*\\)|)(?=[^-]|$)", "i")}, qb = /^(?:input|select|textarea|button)$/i, rb = /^h\d$/i, sb = /^[^{]+\{\s*\[native \w/, tb = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, ub = /[+~]/, vb = /'|\\/g, wb = new RegExp("\\\\([\\da-f]{1,6}" + eb + "?|(" + eb + ")|.)", "ig"), xb = function(a, b, c) {
       var d = "0x" + b - 65536;
       return d !== d || c ? b : 0 > d ? String.fromCharCode(d + 65536) : String.fromCharCode(55296 | d >> 10, 56320 | 1023 & d)
   };
   try {
       ab.apply(Z = bb.call(P.childNodes), P.childNodes), Z[P.childNodes.length].nodeType
   } catch (yb) {
       ab = {apply: Z.length ? function(a, b) {
               _.apply(a, bb.call(b))
           } : function(a, b) {
               for (var c = a.length, d = 0; a[c++] = b[d++]; )
                   ;
               a.length = c - 1
           }}
   }
   x = b.support = {}, B = b.isXML = function(a) {
       var b = a && (a.ownerDocument || a).documentElement;
       return b ? "HTML" !== b.nodeName : !1
   }, G = b.setDocument = function(a) {
       var b, c = a ? a.ownerDocument || a : P, d = c.defaultView;
       return c !== H && 9 === c.nodeType && c.documentElement ? (H = c, I = c.documentElement, J = !B(c), d && d.attachEvent && d !== d.top && d.attachEvent("onbeforeunload", function() {
           G()
       }), x.attributes = e(function(a) {
           return a.className = "i", !a.getAttribute("className")
       }), x.getElementsByTagName = e(function(a) {
           return a.appendChild(c.createComment("")), !a.getElementsByTagName("*").length
       }), x.getElementsByClassName = sb.test(c.getElementsByClassName) && e(function(a) {
           return a.innerHTML = "<div class='a'></div><div class='a i'></div>", a.firstChild.className = "i", 2 === a.getElementsByClassName("i").length
       }), x.getById = e(function(a) {
           return I.appendChild(a).id = O, !c.getElementsByName || !c.getElementsByName(O).length
       }), x.getById ? (z.find.ID = function(a, b) {
           if (typeof b.getElementById !== W && J) {
               var c = b.getElementById(a);
               return c && c.parentNode ? [c] : []
           }
       }, z.filter.ID = function(a) {
           var b = a.replace(wb, xb);
           return function(a) {
               return a.getAttribute("id") === b
           }
       }) : (delete z.find.ID, z.filter.ID = function(a) {
           var b = a.replace(wb, xb);
           return function(a) {
               var c = typeof a.getAttributeNode !== W && a.getAttributeNode("id");
               return c && c.value === b
           }
       }), z.find.TAG = x.getElementsByTagName ? function(a, b) {
           return typeof b.getElementsByTagName !== W ? b.getElementsByTagName(a) : void 0
       } : function(a, b) {
           var c, d = [], e = 0, f = b.getElementsByTagName(a);
           if ("*" === a) {
               for (; c = f[e++]; )
                   1 === c.nodeType && d.push(c);
               return d
           }
           return f
       }, z.find.CLASS = x.getElementsByClassName && function(a, b) {
           return typeof b.getElementsByClassName !== W && J ? b.getElementsByClassName(a) : void 0
       }, L = [], K = [], (x.qsa = sb.test(c.querySelectorAll)) && (e(function(a) {
           a.innerHTML = "<select><option selected=''></option></select>", a.querySelectorAll("[selected]").length || K.push("\\[" + eb + "*(?:value|" + db + ")"), a.querySelectorAll(":checked").length || K.push(":checked")
       }), e(function(a) {
           var b = c.createElement("input");
           b.setAttribute("type", "hidden"), a.appendChild(b).setAttribute("t", ""), a.querySelectorAll("[t^='']").length && K.push("[*^$]=" + eb + "*(?:''|\"\")"), a.querySelectorAll(":enabled").length || K.push(":enabled", ":disabled"), a.querySelectorAll("*,:x"), K.push(",.*:")
       })), (x.matchesSelector = sb.test(M = I.webkitMatchesSelector || I.mozMatchesSelector || I.oMatchesSelector || I.msMatchesSelector)) && e(function(a) {
           x.disconnectedMatch = M.call(a, "div"), M.call(a, "[s!='']:x"), L.push("!=", ib)
       }), K = K.length && new RegExp(K.join("|")), L = L.length && new RegExp(L.join("|")), b = sb.test(I.compareDocumentPosition), N = b || sb.test(I.contains) ? function(a, b) {
           var c = 9 === a.nodeType ? a.documentElement : a, d = b && b.parentNode;
           return a === d || !(!d || 1 !== d.nodeType || !(c.contains ? c.contains(d) : a.compareDocumentPosition && 16 & a.compareDocumentPosition(d)))
       } : function(a, b) {
           if (b)
               for (; b = b.parentNode; )
                   if (b === a)
                       return !0;
           return !1
       }, V = b ? function(a, b) {
           if (a === b)
               return F = !0, 0;
           var d = !a.compareDocumentPosition - !b.compareDocumentPosition;
           return d ? d : (d = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1, 1 & d || !x.sortDetached && b.compareDocumentPosition(a) === d ? a === c || a.ownerDocument === P && N(P, a) ? -1 : b === c || b.ownerDocument === P && N(P, b) ? 1 : E ? cb.call(E, a) - cb.call(E, b) : 0 : 4 & d ? -1 : 1)
       } : function(a, b) {
           if (a === b)
               return F = !0, 0;
           var d, e = 0, f = a.parentNode, h = b.parentNode, i = [a], j = [b];
           if (!f || !h)
               return a === c ? -1 : b === c ? 1 : f ? -1 : h ? 1 : E ? cb.call(E, a) - cb.call(E, b) : 0;
           if (f === h)
               return g(a, b);
           for (d = a; d = d.parentNode; )
               i.unshift(d);
           for (d = b; d = d.parentNode; )
               j.unshift(d);
           for (; i[e] === j[e]; )
               e++;
           return e ? g(i[e], j[e]) : i[e] === P ? -1 : j[e] === P ? 1 : 0
       }, c) : H
   }, b.matches = function(a, c) {
       return b(a, null, null, c)
   }, b.matchesSelector = function(a, c) {
       if ((a.ownerDocument || a) !== H && G(a), c = c.replace(mb, "='$1']"), !(!x.matchesSelector || !J || L && L.test(c) || K && K.test(c)))
           try {
               var d = M.call(a, c);
               if (d || x.disconnectedMatch || a.document && 11 !== a.document.nodeType)
                   return d
           } catch (e) {
           }
       return b(c, H, null, [a]).length > 0
   }, b.contains = function(a, b) {
       return (a.ownerDocument || a) !== H && G(a), N(a, b)
   }, b.attr = function(a, b) {
       (a.ownerDocument || a) !== H && G(a);
       var c = z.attrHandle[b.toLowerCase()], d = c && Y.call(z.attrHandle, b.toLowerCase()) ? c(a, b, !J) : void 0;
       return void 0 !== d ? d : x.attributes || !J ? a.getAttribute(b) : (d = a.getAttributeNode(b)) && d.specified ? d.value : null
   }, b.error = function(a) {
       throw new Error("Syntax error, unrecognized expression: " + a)
   }, b.uniqueSort = function(a) {
       var b, c = [], d = 0, e = 0;
       if (F = !x.detectDuplicates, E = !x.sortStable && a.slice(0), a.sort(V), F) {
           for (; b = a[e++]; )
               b === a[e] && (d = c.push(e));
           for (; d--; )
               a.splice(c[d], 1)
       }
       return E = null, a
   }, A = b.getText = function(a) {
       var b, c = "", d = 0, e = a.nodeType;
       if (e) {
           if (1 === e || 9 === e || 11 === e) {
               if ("string" == typeof a.textContent)
                   return a.textContent;
               for (a = a.firstChild; a; a = a.nextSibling)
                   c += A(a)
           } else if (3 === e || 4 === e)
               return a.nodeValue
       } else
           for (; b = a[d++]; )
               c += A(b);
       return c
   }, z = b.selectors = {cacheLength: 50,createPseudo: d,match: pb,attrHandle: {},find: {},relative: {">": {dir: "parentNode",first: !0}," ": {dir: "parentNode"},"+": {dir: "previousSibling",first: !0},"~": {dir: "previousSibling"}},preFilter: {ATTR: function(a) {
               return a[1] = a[1].replace(wb, xb), a[3] = (a[4] || a[5] || "").replace(wb, xb), "~=" === a[2] && (a[3] = " " + a[3] + " "), a.slice(0, 4)
           },CHILD: function(a) {
               return a[1] = a[1].toLowerCase(), "nth" === a[1].slice(0, 3) ? (a[3] || b.error(a[0]), a[4] = +(a[4] ? a[5] + (a[6] || 1) : 2 * ("even" === a[3] || "odd" === a[3])), a[5] = +(a[7] + a[8] || "odd" === a[3])) : a[3] && b.error(a[0]), a
           },PSEUDO: function(a) {
               var b, c = !a[5] && a[2];
               return pb.CHILD.test(a[0]) ? null : (a[3] && void 0 !== a[4] ? a[2] = a[4] : c && nb.test(c) && (b = m(c, !0)) && (b = c.indexOf(")", c.length - b) - c.length) && (a[0] = a[0].slice(0, b), a[2] = c.slice(0, b)), a.slice(0, 3))
           }},filter: {TAG: function(a) {
               var b = a.replace(wb, xb).toLowerCase();
               return "*" === a ? function() {
                   return !0
               } : function(a) {
                   return a.nodeName && a.nodeName.toLowerCase() === b
               }
           },CLASS: function(a) {
               var b = S[a + " "];
               return b || (b = new RegExp("(^|" + eb + ")" + a + "(" + eb + "|$)")) && S(a, function(a) {
                   return b.test("string" == typeof a.className && a.className || typeof a.getAttribute !== W && a.getAttribute("class") || "")
               })
           },ATTR: function(a, c, d) {
               return function(e) {
                   var f = b.attr(e, a);
                   return null == f ? "!=" === c : c ? (f += "", "=" === c ? f === d : "!=" === c ? f !== d : "^=" === c ? d && 0 === f.indexOf(d) : "*=" === c ? d && f.indexOf(d) > -1 : "$=" === c ? d && f.slice(-d.length) === d : "~=" === c ? (" " + f + " ").indexOf(d) > -1 : "|=" === c ? f === d || f.slice(0, d.length + 1) === d + "-" : !1) : !0
               }
           },CHILD: function(a, b, c, d, e) {
               var f = "nth" !== a.slice(0, 3), g = "last" !== a.slice(-4), h = "of-type" === b;
               return 1 === d && 0 === e ? function(a) {
                   return !!a.parentNode
               } : function(b, c, i) {
                   var j, k, l, m, n, o, p = f !== g ? "nextSibling" : "previousSibling", q = b.parentNode, r = h && b.nodeName.toLowerCase(), s = !i && !h;
                   if (q) {
                       if (f) {
                           for (; p; ) {
                               for (l = b; l = l[p]; )
                                   if (h ? l.nodeName.toLowerCase() === r : 1 === l.nodeType)
                                       return !1;
                               o = p = "only" === a && !o && "nextSibling"
                           }
                           return !0
                       }
                       if (o = [g ? q.firstChild : q.lastChild], g && s) {
                           for (k = q[O] || (q[O] = {}), j = k[a] || [], n = j[0] === Q && j[1], m = j[0] === Q && j[2], l = n && q.childNodes[n]; l = ++n && l && l[p] || (m = n = 0) || o.pop(); )
                               if (1 === l.nodeType && ++m && l === b) {
                                   k[a] = [Q, n, m];
                                   break
                               }
                       } else if (s && (j = (b[O] || (b[O] = {}))[a]) && j[0] === Q)
                           m = j[1];
                       else
                           for (; (l = ++n && l && l[p] || (m = n = 0) || o.pop()) && ((h ? l.nodeName.toLowerCase() !== r : 1 !== l.nodeType) || !++m || (s && ((l[O] || (l[O] = {}))[a] = [Q, m]), l !== b)); )
                               ;
                       return m -= e, m === d || 0 === m % d && m / d >= 0
                   }
               }
           },PSEUDO: function(a, c) {
               var e, f = z.pseudos[a] || z.setFilters[a.toLowerCase()] || b.error("unsupported pseudo: " + a);
               return f[O] ? f(c) : f.length > 1 ? (e = [a, a, "", c], z.setFilters.hasOwnProperty(a.toLowerCase()) ? d(function(a, b) {
                   for (var d, e = f(a, c), g = e.length; g--; )
                       d = cb.call(a, e[g]), a[d] = !(b[d] = e[g])
               }) : function(a) {
                   return f(a, 0, e)
               }) : f
           }},pseudos: {not: d(function(a) {
               var b = [], c = [], e = C(a.replace(jb, "$1"));
               return e[O] ? d(function(a, b, c, d) {
                   for (var f, g = e(a, null, d, []), h = a.length; h--; )
                       (f = g[h]) && (a[h] = !(b[h] = f))
               }) : function(a, d, f) {
                   return b[0] = a, e(b, null, f, c), !c.pop()
               }
           }),has: d(function(a) {
               return function(c) {
                   return b(a, c).length > 0
               }
           }),contains: d(function(a) {
               return function(b) {
                   return (b.textContent || b.innerText || A(b)).indexOf(a) > -1
               }
           }),lang: d(function(a) {
               return ob.test(a || "") || b.error("unsupported lang: " + a), a = a.replace(wb, xb).toLowerCase(), function(b) {
                   var c;
                   do
                       if (c = J ? b.lang : b.getAttribute("xml:lang") || b.getAttribute("lang"))
                           return c = c.toLowerCase(), c === a || 0 === c.indexOf(a + "-");
                   while ((b = b.parentNode) && 1 === b.nodeType);
                   return !1
               }
           }),target: function(b) {
               var c = a.location && a.location.hash;
               return c && c.slice(1) === b.id
           },root: function(a) {
               return a === I
           },focus: function(a) {
               return a === H.activeElement && (!H.hasFocus || H.hasFocus()) && !!(a.type || a.href || ~a.tabIndex)
           },enabled: function(a) {
               return a.disabled === !1
           },disabled: function(a) {
               return a.disabled === !0
           },checked: function(a) {
               var b = a.nodeName.toLowerCase();
               return "input" === b && !!a.checked || "option" === b && !!a.selected
           },selected: function(a) {
               return a.parentNode && a.parentNode.selectedIndex, a.selected === !0
           },empty: function(a) {
               for (a = a.firstChild; a; a = a.nextSibling)
                   if (a.nodeType < 6)
                       return !1;
               return !0
           },parent: function(a) {
               return !z.pseudos.empty(a)
           },header: function(a) {
               return rb.test(a.nodeName)
           },input: function(a) {
               return qb.test(a.nodeName)
           },button: function(a) {
               var b = a.nodeName.toLowerCase();
               return "input" === b && "button" === a.type || "button" === b
           },text: function(a) {
               var b;
               return "input" === a.nodeName.toLowerCase() && "text" === a.type && (null == (b = a.getAttribute("type")) || b.toLowerCase() === a.type)
           },first: j(function() {
               return [0]
           }),last: j(function(a, b) {
               return [b - 1]
           }),eq: j(function(a, b, c) {
               return [0 > c ? c + b : c]
           }),even: j(function(a, b) {
               for (var c = 0; b > c; c += 2)
                   a.push(c);
               return a
           }),odd: j(function(a, b) {
               for (var c = 1; b > c; c += 2)
                   a.push(c);
               return a
           }),lt: j(function(a, b, c) {
               for (var d = 0 > c ? c + b : c; --d >= 0; )
                   a.push(d);
               return a
           }),gt: j(function(a, b, c) {
               for (var d = 0 > c ? c + b : c; ++d < b; )
                   a.push(d);
               return a
           })}}, z.pseudos.nth = z.pseudos.eq;
   for (w in {radio: !0,checkbox: !0,file: !0,password: !0,image: !0})
       z.pseudos[w] = h(w);
   for (w in {submit: !0,reset: !0})
       z.pseudos[w] = i(w);
   l.prototype = z.filters = z.pseudos, z.setFilters = new l, C = b.compile = function(a, b) {
       var c, d = [], e = [], f = U[a + " "];
       if (!f) {
           for (b || (b = m(a)), c = b.length; c--; )
               f = s(b[c]), f[O] ? d.push(f) : e.push(f);
           f = U(a, t(e, d))
       }
       return f
   }, x.sortStable = O.split("").sort(V).join("") === O, x.detectDuplicates = !!F, G(), x.sortDetached = e(function(a) {
       return 1 & a.compareDocumentPosition(H.createElement("div"))
   }), e(function(a) {
       return a.innerHTML = "<a href='#'></a>", "#" === a.firstChild.getAttribute("href")
   }) || f("type|href|height|width", function(a, b, c) {
       return c ? void 0 : a.getAttribute(b, "type" === b.toLowerCase() ? 1 : 2)
   }), x.attributes && e(function(a) {
       return a.innerHTML = "<input/>", a.firstChild.setAttribute("value", ""), "" === a.firstChild.getAttribute("value")
   }) || f("value", function(a, b, c) {
       return c || "input" !== a.nodeName.toLowerCase() ? void 0 : a.defaultValue
   }), e(function(a) {
       return null == a.getAttribute("disabled")
   }) || f(db, function(a, b, c) {
       var d;
       return c ? void 0 : a[b] === !0 ? b.toLowerCase() : (d = a.getAttributeNode(b)) && d.specified ? d.value : null
   }), "function" == typeof define && define.amd ? define(function() {
       return b
   }) : "undefined" != typeof module && module.exports ? module.exports = b : a.Sizzle = b
}(window);
//     Underscore.js 1.5.2
//     http://underscorejs.org
//     (c) 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.
(function() {
   var n = this, t = n._, r = {}, e = Array.prototype, u = Object.prototype, i = Function.prototype, a = e.push, o = e.slice, c = e.concat, l = u.toString, f = u.hasOwnProperty, s = e.forEach, p = e.map, h = e.reduce, v = e.reduceRight, g = e.filter, d = e.every, m = e.some, y = e.indexOf, b = e.lastIndexOf, x = Array.isArray, w = Object.keys, _ = i.bind, j = function(n) {
       return n instanceof j ? n : this instanceof j ? (this._wrapped = n, void 0) : new j(n)
   };
   "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = j), exports._ = j) : n._ = j, j.VERSION = "1.5.2";
   var A = j.each = j.forEach = function(n, t, e) {
       if (null != n)
           if (s && n.forEach === s)
               n.forEach(t, e);
           else if (n.length === +n.length) {
               for (var u = 0, i = n.length; i > u; u++)
                   if (t.call(e, n[u], u, n) === r)
                       return
           } else
               for (var a = j.keys(n), u = 0, i = a.length; i > u; u++)
                   if (t.call(e, n[a[u]], a[u], n) === r)
                       return
   };
   j.map = j.collect = function(n, t, r) {
       var e = [];
       return null == n ? e : p && n.map === p ? n.map(t, r) : (A(n, function(n, u, i) {
           e.push(t.call(r, n, u, i))
       }), e)
   };
   var E = "Reduce of empty array with no initial value";
   j.reduce = j.foldl = j.inject = function(n, t, r, e) {
       var u = arguments.length > 2;
       if (null == n && (n = []), h && n.reduce === h)
           return e && (t = j.bind(t, e)), u ? n.reduce(t, r) : n.reduce(t);
       if (A(n, function(n, i, a) {
           u ? r = t.call(e, r, n, i, a) : (r = n, u = !0)
       }), !u)
           throw new TypeError(E);
       return r
   }, j.reduceRight = j.foldr = function(n, t, r, e) {
       var u = arguments.length > 2;
       if (null == n && (n = []), v && n.reduceRight === v)
           return e && (t = j.bind(t, e)), u ? n.reduceRight(t, r) : n.reduceRight(t);
       var i = n.length;
       if (i !== +i) {
           var a = j.keys(n);
           i = a.length
       }
       if (A(n, function(o, c, l) {
           c = a ? a[--i] : --i, u ? r = t.call(e, r, n[c], c, l) : (r = n[c], u = !0)
       }), !u)
           throw new TypeError(E);
       return r
   }, j.find = j.detect = function(n, t, r) {
       var e;
       return O(n, function(n, u, i) {
           return t.call(r, n, u, i) ? (e = n, !0) : void 0
       }), e
   }, j.filter = j.select = function(n, t, r) {
       var e = [];
       return null == n ? e : g && n.filter === g ? n.filter(t, r) : (A(n, function(n, u, i) {
           t.call(r, n, u, i) && e.push(n)
       }), e)
   }, j.reject = function(n, t, r) {
       return j.filter(n, function(n, e, u) {
           return !t.call(r, n, e, u)
       }, r)
   }, j.every = j.all = function(n, t, e) {
       t || (t = j.identity);
       var u = !0;
       return null == n ? u : d && n.every === d ? n.every(t, e) : (A(n, function(n, i, a) {
           return (u = u && t.call(e, n, i, a)) ? void 0 : r
       }), !!u)
   };
   var O = j.some = j.any = function(n, t, e) {
       t || (t = j.identity);
       var u = !1;
       return null == n ? u : m && n.some === m ? n.some(t, e) : (A(n, function(n, i, a) {
           return u || (u = t.call(e, n, i, a)) ? r : void 0
       }), !!u)
   };
   j.contains = j.include = function(n, t) {
       return null == n ? !1 : y && n.indexOf === y ? n.indexOf(t) != -1 : O(n, function(n) {
           return n === t
       })
   }, j.invoke = function(n, t) {
       var r = o.call(arguments, 2), e = j.isFunction(t);
       return j.map(n, function(n) {
           return (e ? t : n[t]).apply(n, r)
       })
   }, j.pluck = function(n, t) {
       return j.map(n, function(n) {
           return n[t]
       })
   }, j.where = function(n, t, r) {
       return j.isEmpty(t) ? r ? void 0 : [] : j[r ? "find" : "filter"](n, function(n) {
           for (var r in t)
               if (t[r] !== n[r])
                   return !1;
           return !0
       })
   }, j.findWhere = function(n, t) {
       return j.where(n, t, !0)
   }, j.max = function(n, t, r) {
       if (!t && j.isArray(n) && n[0] === +n[0] && n.length < 65535)
           return Math.max.apply(Math, n);
       if (!t && j.isEmpty(n))
           return -1 / 0;
       var e = {computed: -1 / 0,value: -1 / 0};
       return A(n, function(n, u, i) {
           var a = t ? t.call(r, n, u, i) : n;
           a > e.computed && (e = {value: n,computed: a})
       }), e.value
   }, j.min = function(n, t, r) {
       if (!t && j.isArray(n) && n[0] === +n[0] && n.length < 65535)
           return Math.min.apply(Math, n);
       if (!t && j.isEmpty(n))
           return 1 / 0;
       var e = {computed: 1 / 0,value: 1 / 0};
       return A(n, function(n, u, i) {
           var a = t ? t.call(r, n, u, i) : n;
           a < e.computed && (e = {value: n,computed: a})
       }), e.value
   }, j.shuffle = function(n) {
       var t, r = 0, e = [];
       return A(n, function(n) {
           t = j.random(r++), e[r - 1] = e[t], e[t] = n
       }), e
   }, j.sample = function(n, t, r) {
       return arguments.length < 2 || r ? n[j.random(n.length - 1)] : j.shuffle(n).slice(0, Math.max(0, t))
   };
   var k = function(n) {
       return j.isFunction(n) ? n : function(t) {
           return t[n]
       }
   };
   j.sortBy = function(n, t, r) {
       var e = k(t);
       return j.pluck(j.map(n, function(n, t, u) {
           return {value: n,index: t,criteria: e.call(r, n, t, u)}
       }).sort(function(n, t) {
           var r = n.criteria, e = t.criteria;
           if (r !== e) {
               if (r > e || r === void 0)
                   return 1;
               if (e > r || e === void 0)
                   return -1
           }
           return n.index - t.index
       }), "value")
   };
   var F = function(n) {
       return function(t, r, e) {
           var u = {}, i = null == r ? j.identity : k(r);
           return A(t, function(r, a) {
               var o = i.call(e, r, a, t);
               n(u, o, r)
           }), u
       }
   };
   j.groupBy = F(function(n, t, r) {
       (j.has(n, t) ? n[t] : n[t] = []).push(r)
   }), j.indexBy = F(function(n, t, r) {
       n[t] = r
   }), j.countBy = F(function(n, t) {
       j.has(n, t) ? n[t]++ : n[t] = 1
   }), j.sortedIndex = function(n, t, r, e) {
       r = null == r ? j.identity : k(r);
       for (var u = r.call(e, t), i = 0, a = n.length; a > i; ) {
           var o = i + a >>> 1;
           r.call(e, n[o]) < u ? i = o + 1 : a = o
       }
       return i
   }, j.toArray = function(n) {
       return n ? j.isArray(n) ? o.call(n) : n.length === +n.length ? j.map(n, j.identity) : j.values(n) : []
   }, j.size = function(n) {
       return null == n ? 0 : n.length === +n.length ? n.length : j.keys(n).length
   }, j.first = j.head = j.take = function(n, t, r) {
       return null == n ? void 0 : null == t || r ? n[0] : o.call(n, 0, t)
   }, j.initial = function(n, t, r) {
       return o.call(n, 0, n.length - (null == t || r ? 1 : t))
   }, j.last = function(n, t, r) {
       return null == n ? void 0 : null == t || r ? n[n.length - 1] : o.call(n, Math.max(n.length - t, 0))
   }, j.rest = j.tail = j.drop = function(n, t, r) {
       return o.call(n, null == t || r ? 1 : t)
   }, j.compact = function(n) {
       return j.filter(n, j.identity)
   };
   var M = function(n, t, r) {
       return t && j.every(n, j.isArray) ? c.apply(r, n) : (A(n, function(n) {
           j.isArray(n) || j.isArguments(n) ? t ? a.apply(r, n) : M(n, t, r) : r.push(n)
       }), r)
   };
   j.flatten = function(n, t) {
       return M(n, t, [])
   }, j.without = function(n) {
       return j.difference(n, o.call(arguments, 1))
   }, j.uniq = j.unique = function(n, t, r, e) {
       j.isFunction(t) && (e = r, r = t, t = !1);
       var u = r ? j.map(n, r, e) : n, i = [], a = [];
       return A(u, function(r, e) {
           (t ? e && a[a.length - 1] === r : j.contains(a, r)) || (a.push(r), i.push(n[e]))
       }), i
   }, j.union = function() {
       return j.uniq(j.flatten(arguments, !0))
   }, j.intersection = function(n) {
       var t = o.call(arguments, 1);
       return j.filter(j.uniq(n), function(n) {
           return j.every(t, function(t) {
               return j.indexOf(t, n) >= 0
           })
       })
   }, j.difference = function(n) {
       var t = c.apply(e, o.call(arguments, 1));
       return j.filter(n, function(n) {
           return !j.contains(t, n)
       })
   }, j.zip = function() {
       for (var n = j.max(j.pluck(arguments, "length").concat(0)), t = new Array(n), r = 0; n > r; r++)
           t[r] = j.pluck(arguments, "" + r);
       return t
   }, j.object = function(n, t) {
       if (null == n)
           return {};
       for (var r = {}, e = 0, u = n.length; u > e; e++)
           t ? r[n[e]] = t[e] : r[n[e][0]] = n[e][1];
       return r
   }, j.indexOf = function(n, t, r) {
       if (null == n)
           return -1;
       var e = 0, u = n.length;
       if (r) {
           if ("number" != typeof r)
               return e = j.sortedIndex(n, t), n[e] === t ? e : -1;
           e = 0 > r ? Math.max(0, u + r) : r
       }
       if (y && n.indexOf === y)
           return n.indexOf(t, r);
       for (; u > e; e++)
           if (n[e] === t)
               return e;
       return -1
   }, j.lastIndexOf = function(n, t, r) {
       if (null == n)
           return -1;
       var e = null != r;
       if (b && n.lastIndexOf === b)
           return e ? n.lastIndexOf(t, r) : n.lastIndexOf(t);
       for (var u = e ? r : n.length; u--; )
           if (n[u] === t)
               return u;
       return -1
   }, j.range = function(n, t, r) {
       arguments.length <= 1 && (t = n || 0, n = 0), r = arguments[2] || 1;
       for (var e = Math.max(Math.ceil((t - n) / r), 0), u = 0, i = new Array(e); e > u; )
           i[u++] = n, n += r;
       return i
   };
   var R = function() {
   };
   j.bind = function(n, t) {
       var r, e;
       if (_ && n.bind === _)
           return _.apply(n, o.call(arguments, 1));
       if (!j.isFunction(n))
           throw new TypeError;
       return r = o.call(arguments, 2), e = function() {
           if (!(this instanceof e))
               return n.apply(t, r.concat(o.call(arguments)));
           R.prototype = n.prototype;
           var u = new R;
           R.prototype = null;
           var i = n.apply(u, r.concat(o.call(arguments)));
           return Object(i) === i ? i : u
       }
   }, j.partial = function(n) {
       var t = o.call(arguments, 1);
       return function() {
           return n.apply(this, t.concat(o.call(arguments)))
       }
   }, j.bindAll = function(n) {
       var t = o.call(arguments, 1);
       if (0 === t.length)
           throw new Error("bindAll must be passed function names");
       return A(t, function(t) {
           n[t] = j.bind(n[t], n)
       }), n
   }, j.memoize = function(n, t) {
       var r = {};
       return t || (t = j.identity), function() {
           var e = t.apply(this, arguments);
           return j.has(r, e) ? r[e] : r[e] = n.apply(this, arguments)
       }
   }, j.delay = function(n, t) {
       var r = o.call(arguments, 2);
       return setTimeout(function() {
           return n.apply(null, r)
       }, t)
   }, j.defer = function(n) {
       return j.delay.apply(j, [n, 1].concat(o.call(arguments, 1)))
   }, j.throttle = function(n, t, r) {
       var e, u, i, a = null, o = 0;
       r || (r = {});
       var c = function() {
           o = r.leading === !1 ? 0 : new Date, a = null, i = n.apply(e, u)
       };
       return function() {
           var l = new Date;
           o || r.leading !== !1 || (o = l);
           var f = t - (l - o);
           return e = this, u = arguments, 0 >= f ? (clearTimeout(a), a = null, o = l, i = n.apply(e, u)) : a || r.trailing === !1 || (a = setTimeout(c, f)), i
       }
   }, j.debounce = function(n, t, r) {
       var e, u, i, a, o;
       return function() {
           i = this, u = arguments, a = new Date;
           var c = function() {
               var l = new Date - a;
               t > l ? e = setTimeout(c, t - l) : (e = null, r || (o = n.apply(i, u)))
           }, l = r && !e;
           return e || (e = setTimeout(c, t)), l && (o = n.apply(i, u)), o
       }
   }, j.once = function(n) {
       var t, r = !1;
       return function() {
           return r ? t : (r = !0, t = n.apply(this, arguments), n = null, t)
       }
   }, j.wrap = function(n, t) {
       return function() {
           var r = [n];
           return a.apply(r, arguments), t.apply(this, r)
       }
   }, j.compose = function() {
       var n = arguments;
       return function() {
           for (var t = arguments, r = n.length - 1; r >= 0; r--)
               t = [n[r].apply(this, t)];
           return t[0]
       }
   }, j.after = function(n, t) {
       return function() {
           return --n < 1 ? t.apply(this, arguments) : void 0
       }
   }, j.keys = w || function(n) {
       if (n !== Object(n))
           throw new TypeError("Invalid object");
       var t = [];
       for (var r in n)
           j.has(n, r) && t.push(r);
       return t
   }, j.values = function(n) {
       for (var t = j.keys(n), r = t.length, e = new Array(r), u = 0; r > u; u++)
           e[u] = n[t[u]];
       return e
   }, j.pairs = function(n) {
       for (var t = j.keys(n), r = t.length, e = new Array(r), u = 0; r > u; u++)
           e[u] = [t[u], n[t[u]]];
       return e
   }, j.invert = function(n) {
       for (var t = {}, r = j.keys(n), e = 0, u = r.length; u > e; e++)
           t[n[r[e]]] = r[e];
       return t
   }, j.functions = j.methods = function(n) {
       var t = [];
       for (var r in n)
           j.isFunction(n[r]) && t.push(r);
       return t.sort()
   }, j.extend = function(n) {
       return A(o.call(arguments, 1), function(t) {
           if (t)
               for (var r in t)
                   n[r] = t[r]
       }), n
   }, j.pick = function(n) {
       var t = {}, r = c.apply(e, o.call(arguments, 1));
       return A(r, function(r) {
           r in n && (t[r] = n[r])
       }), t
   }, j.omit = function(n) {
       var t = {}, r = c.apply(e, o.call(arguments, 1));
       for (var u in n)
           j.contains(r, u) || (t[u] = n[u]);
       return t
   }, j.defaults = function(n) {
       return A(o.call(arguments, 1), function(t) {
           if (t)
               for (var r in t)
                   n[r] === void 0 && (n[r] = t[r])
       }), n
   }, j.clone = function(n) {
       return j.isObject(n) ? j.isArray(n) ? n.slice() : j.extend({}, n) : n
   }, j.tap = function(n, t) {
       return t(n), n
   };
   var S = function(n, t, r, e) {
       if (n === t)
           return 0 !== n || 1 / n == 1 / t;
       if (null == n || null == t)
           return n === t;
       n instanceof j && (n = n._wrapped), t instanceof j && (t = t._wrapped);
       var u = l.call(n);
       if (u != l.call(t))
           return !1;
       switch (u) {
           case "[object String]":
               return n == String(t);
           case "[object Number]":
               return n != +n ? t != +t : 0 == n ? 1 / n == 1 / t : n == +t;
           case "[object Date]":
           case "[object Boolean]":
               return +n == +t;
           case "[object RegExp]":
               return n.source == t.source && n.global == t.global && n.multiline == t.multiline && n.ignoreCase == t.ignoreCase
       }
       if ("object" != typeof n || "object" != typeof t)
           return !1;
       for (var i = r.length; i--; )
           if (r[i] == n)
               return e[i] == t;
       var a = n.constructor, o = t.constructor;
       if (a !== o && !(j.isFunction(a) && a instanceof a && j.isFunction(o) && o instanceof o))
           return !1;
       r.push(n), e.push(t);
       var c = 0, f = !0;
       if ("[object Array]" == u) {
           if (c = n.length, f = c == t.length)
               for (; c-- && (f = S(n[c], t[c], r, e)); )
                   ;
       } else {
           for (var s in n)
               if (j.has(n, s) && (c++, !(f = j.has(t, s) && S(n[s], t[s], r, e))))
                   break;
           if (f) {
               for (s in t)
                   if (j.has(t, s) && !c--)
                       break;
               f = !c
           }
       }
       return r.pop(), e.pop(), f
   };
   j.isEqual = function(n, t) {
       return S(n, t, [], [])
   }, j.isEmpty = function(n) {
       if (null == n)
           return !0;
       if (j.isArray(n) || j.isString(n))
           return 0 === n.length;
       for (var t in n)
           if (j.has(n, t))
               return !1;
       return !0
   }, j.isElement = function(n) {
       return !(!n || 1 !== n.nodeType)
   }, j.isArray = x || function(n) {
       return "[object Array]" == l.call(n)
   }, j.isObject = function(n) {
       return n === Object(n)
   }, A(["Arguments", "Function", "String", "Number", "Date", "RegExp"], function(n) {
       j["is" + n] = function(t) {
           return l.call(t) == "[object " + n + "]"
       }
   }), j.isArguments(arguments) || (j.isArguments = function(n) {
       return !(!n || !j.has(n, "callee"))
   }), "function" != typeof /./ && (j.isFunction = function(n) {
       return "function" == typeof n
   }), j.isFinite = function(n) {
       return isFinite(n) && !isNaN(parseFloat(n))
   }, j.isNaN = function(n) {
       return j.isNumber(n) && n != +n
   }, j.isBoolean = function(n) {
       return n === !0 || n === !1 || "[object Boolean]" == l.call(n)
   }, j.isNull = function(n) {
       return null === n
   }, j.isUndefined = function(n) {
       return n === void 0
   }, j.has = function(n, t) {
       return f.call(n, t)
   }, j.noConflict = function() {
       return n._ = t, this
   }, j.identity = function(n) {
       return n
   }, j.times = function(n, t, r) {
       for (var e = Array(Math.max(0, n)), u = 0; n > u; u++)
           e[u] = t.call(r, u);
       return e
   }, j.random = function(n, t) {
       return null == t && (t = n, n = 0), n + Math.floor(Math.random() * (t - n + 1))
   };
   var I = {escape: {"&": "&amp;","<": "&lt;",">": "&gt;",'"': "&quot;","'": "&#x27;"}};
   I.unescape = j.invert(I.escape);
   var T = {escape: new RegExp("[" + j.keys(I.escape).join("") + "]", "g"),unescape: new RegExp("(" + j.keys(I.unescape).join("|") + ")", "g")};
   j.each(["escape", "unescape"], function(n) {
       j[n] = function(t) {
           return null == t ? "" : ("" + t).replace(T[n], function(t) {
               return I[n][t]
           })
       }
   }), j.result = function(n, t) {
       if (null == n)
           return void 0;
       var r = n[t];
       return j.isFunction(r) ? r.call(n) : r
   }, j.mixin = function(n) {
       A(j.functions(n), function(t) {
           var r = j[t] = n[t];
           j.prototype[t] = function() {
               var n = [this._wrapped];
               return a.apply(n, arguments), z.call(this, r.apply(j, n))
           }
       })
   };
   var N = 0;
   j.uniqueId = function(n) {
       var t = ++N + "";
       return n ? n + t : t
   }, j.templateSettings = {evaluate: /<%([\s\S]+?)%>/g,interpolate: /<%=([\s\S]+?)%>/g,escape: /<%-([\s\S]+?)%>/g};
   var q = /(.)^/, B = {"'": "'","\\": "\\","\r": "r","\n": "n","   ": "t","\u2028": "u2028","\u2029": "u2029"}, D = /\\|'|\r|\n|\t|\u2028|\u2029/g;
   j.template = function(n, t, r) {
       var e;
       r = j.defaults({}, r, j.templateSettings);
       var u = new RegExp([(r.escape || q).source, (r.interpolate || q).source, (r.evaluate || q).source].join("|") + "|$", "g"), i = 0, a = "__p+='";
       n.replace(u, function(t, r, e, u, o) {
           return a += n.slice(i, o).replace(D, function(n) {
               return "\\" + B[n]
           }), r && (a += "'+\n((__t=(" + r + "))==null?'':_.escape(__t))+\n'"), e && (a += "'+\n((__t=(" + e + "))==null?'':__t)+\n'"), u && (a += "';\n" + u + "\n__p+='"), i = o + t.length, t
       }), a += "';\n", r.variable || (a = "with(obj||{}){\n" + a + "}\n"), a = "var __t,__p='',__j=Array.prototype.join," + "print=function(){__p+=__j.call(arguments,'');};\n" + a + "return __p;\n";
       try {
           e = new Function(r.variable || "obj", "_", a)
       } catch (o) {
           throw o.source = a, o
       }
       if (t)
           return e(t, j);
       var c = function(n) {
           return e.call(this, n, j)
       };
       return c.source = "function(" + (r.variable || "obj") + "){\n" + a + "}", c
   }, j.chain = function(n) {
       return j(n).chain()
   };
   var z = function(n) {
       return this._chain ? j(n).chain() : n
   };
   j.mixin(j), A(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function(n) {
       var t = e[n];
       j.prototype[n] = function() {
           var r = this._wrapped;
           return t.apply(r, arguments), "shift" != n && "splice" != n || 0 !== r.length || delete r[0], z.call(this, r)
       }
   }), A(["concat", "join", "slice"], function(n) {
       var t = e[n];
       j.prototype[n] = function() {
           return z.call(this, t.apply(this._wrapped, arguments))
       }
   }), j.extend(j.prototype, {chain: function() {
           return this._chain = !0, this
       },value: function() {
           return this._wrapped
       }})
}).call(this);
//# sourceMappingURL=underscore-min.map
/**/FBJCModal = function(a, b) {
   var c = {top: 'auto',autoOpen: false,overlayOpacity: 0.5,overlayColor: '#fff',overlayClose: true,headColor: '#ef6849',bodyColor: '#f3f3f3',titleColor: '#fff',closeButtonClass: 'close',zIndex: 9999,title: '',bodyText: '',fbBox: ''};
   this.options = _.extend(c, a);
   this.fbjc = b;
   return this
};
FBJCModal.prototype = {constructor: FBJCModal,_showModal: function() {
       this._backdrop();
       this._modal()
   },_closeModal: function() {
       var b = Sizzle('#FBJC_overlay')[0];
       var m = Sizzle('#FBJC_modal')[0];
       m.parentElement.removeChild(m);
       b.parentElement.removeChild(b)
   },_backdrop: function() {
       o = this.options;
       var a = o.overlayOpacity * 100;
       this.fbjc.style.addRule("#FBJC_overlay", "overflow:auto;z-index:" + o.zIndex + ";background-color:" + o.overlayColor + ";display:block;position:fixed;top:0;left:0;width:100%;height:100%;opacity:" + o.overlayOpacity + ";-ms-filter:\"progid:DXImageTransform.Microsoft.Alpha(Opacity=" + a + ")\";filter:alpha(opacity=" + a + ");");
       var b = document.createElement('div');
       b.id = 'FBJC_overlay';
       document.body.appendChild(b)
   },_modal: function() {
       o = this.options;
       var a = document.createElement('div');
       a.id = 'FBJC_modal';
       a.style.display = 'block';
       a.style.position = 'fixed';
       a.style.zIndex = o.zIndex + 1;
       a.style.MozBoxSizing = "content-box";
       a.style.boxSizing = "content-box";
       a.style.webkitBoxSizing = "content-box";
       a.style.top = parseInt(o.top, 10) > -1 ? o.top + 'px' : 30 + '%';
       a.style.left = 50 + '%';
       a.style.marginLeft = '-200px';
       a.style.width = '400px';
       a.style.boxShadow = '0px 0px 12px 4px rgba(0, 0, 0, 0.11)';
       a.style.backgroundColor = o.bodyColor;
       a.style.borderRadius = '3px';
       a.style.fontFamily = 'Verdana';
       a.innerHTML = '<div class="FBJC_modal_head" style="-moz-box-sizing:content-box;box-sizing:content-box;border-radius:3px 3px 0 0;position:relative;padding:10px;height:20px;background-color:' + o.headColor + ';"><h3 style="float:left;padding:0px;line-height:19px;width:345px;font-size:15px;color:' + o.titleColor + ';margin:0px;">' + o.title + '</h3>' + o.fbBox + '</div><div class="FBJC_modal_body" style="font-size:13px;padding:10px;">' + o.bodyText + '</div>';
       document.body.appendChild(a);
       var b = Sizzle('.FBJC_modal_head')[0];
       b.appendChild(this._closeBtn())
   },_closeBtn: function() {
       var a = this;
       var b = document.createElement('a');
       b.href = 'javascript:;';
       b.style.position = 'absolute';
       b.style.display = 'block';
       b.style.color = '#000';
       b.style.right = '10px';
       b.style.top = '10px';
       b.style.width = '20px';
       b.style.height = '20px';
       b.style.textDecoration = 'none';
       b.style.backgroundImage = 'url(http://www.fblikejacker.in/assets/images/close.png)';
       b.style.backgroundRepeat = 'no-repeat';
       b.style.backgroundPosition = '0 0';
       b.onclick = function(e) {
           if (a.options.timeout) {
               a.fbjc._timeout()
           }
           a._closeModal()
       };
       b.onmouseover = function(e) {
           b.style.backgroundPosition = '0 -20px'
       };
       b.onmouseout = function() {
           b.style.backgroundPosition = '0 0'
       };
       return b
   },_injectFb: function(a) {
       var b = Sizzle('.FBJC_modal_head')[0];
       b.appendChild(a)
   }};
eval(function(p, a, c, k, e, r) {
   e = function(c) {
       return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36))
   };
   if (!''.replace(/^/, String)) {
       while (c--)
           r[e(c)] = k[c] || e(c);
       k = [function(e) {
               return r[e]
           }];
       e = function() {
           return '\\w+'
       };
       c = 1
   }
   ;
   while (c--)
       if (k[c])
           p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]);
   return p
}('9 L=x;6 M(a){7(a.M)l a.M;1N{a=a.1O}1P(a&&a.1Q!==1);l a}7(n 8.y===\'s\'){8.y=(6(){6 y(a){7(n a!==\'s\'){9 b=8.A(\'1R\');b.t=\'12/13\';b.1S=\'1T\';b.14=a}u{9 b=8.A(\'B\');b.t=\'12/13\'}8.15(\'1U\')[0].1V(b);9 c=8.16[8.16.17-1];7(n c.C===\'s\')c.C=C;7(n c.18===\'s\')c.18=c.1W;l c}6 C(a,b,c){7(n c===\'s\')c=4.1X.17;4.1Y(a+\' {\'+b+\'}\',c)}l y})()}19=6(a){a=a.1Z();9 b=/(1a)[ \\/]([\\w.]+)/.D(a)||/(N)[ \\/]([\\w.]+)/.D(a)||/(20)(?:.*G|)[ \\/]([\\w.]+)/.D(a)||/(21) ([\\w.]+)/.D(a)||a.22("23")<0&&/(24)(?:.*? 25:([\\w.]+)|)/.D(a)||[];l{j:b[1]||"",G:b[2]||"0"}};7(!L){H=19(26.27);j={};7(H.j){j[H.j]=m;j.G=H.G}7(j.1a){j.N=m}u 7(j.N){j.28=m}L=j}6 O(a){1b={P:\'\',Q:"29",R:"2a",E:"2b",2c:"2d",2e:"2f",2g:F,2h:"2i",I:\'\',S:\'\',1c:m,g:x,1d:2j,T:0,t:\'i\',i:{}};4.5=1e.1f(1b,a);4.5.i=1e.1f(4.5.i,{g:4.5.g});4.i=U 2k(4.5.i,4);4.1g()}O.2l={2m:O,1g:6(){9 e=4;4.B=8.y();4.1h();4.1i(6(r){7(r){e.1j();7(e.5.t==="2n")e.2o();u{e.1k()}}})},1h:6(a){9 b,V=8.15(\'1l\')[0];7(8.v(\'J-W\'))l;b=8.A(\'1l\');b.X=\'J-W\';b.Y="//2p.J.2q/"+4.5.Q+"/2r.2s";b.2t=b.2u=6(){9 k=b.2v;7((!k||/2w|2x/.2y(k))){}};V.2z.1m(b,V);l},1k:6(){9 e=4.5;9 a=8.A("a");9 b=4.5.g==="0"||4.5.g===0?"2A":"2B";9 c=4.5.g==="0"||4.5.g===0?"2C":"2D";9 f=4.5.g==="0"||4.5.g===0?"2E":"1n";4.B.C("#K","E:"+b+" !2F;1o:"+c+";2G:"+f+";2H:1n;2I:2J;2K:2L;2M:2N;z-2O:2P;1p:0;-2Q-1q:\\"2R:2S.2T.2U(2V=0)\\";1q:2W(1p=0);2X:2Y;");a.X=\'K\';7(4.5.g==="0"||4.5.g===0||4.5.g==="1"||4.5.g===1){a.1r=\'1s-1t\'}u{a.1r=\'1s-2Z\'}a.30="<3";a.o(\'p-14\',4.5.I);a.o(\'p-31\',\'x\');a.o(\'p-R\',4.5.R);a.o(\'p-32-33\',\'x\');a.o(\'p-E\',4.5.E);a.o(\'p-34\',\'1t\');a.o(\'p-35\',4.5.Q);a.o(\'p-36\',\'x\');7(4.5.g==="1"||4.5.g===1||4.5.g===2||4.5.g==="2"){a.37=6(){7(n 4.1u==="s"){4.1u=h.1v(6(){d=8.v(\'K\');7(d!==F){d.1w.1x(d)}},e.1d)}}}7(e.T){Z=4;h.1v(6(){Z.i.1y();Z.i.1z(a);h.q.1A.1B(a[0])},e.T*38)}u{4.i.1y();4.i.1z(a);h.q.1A.1B(a[0])}},1j:6(){9 a=8.A("39");a.X="10";a.B.E=0;a.B.1o=0;8.1C.1m(a,8.1C.3a[0])},1i:6(a){9 e=4;9 c=6(){h.q.3b({P:(e.5&&e.5.P)||F,1D:m,3c:m,3d:m});h.q.1E.1F("1G.3e",6(f){e.1H(f)});h.q.1E.1F("1G.3f",6(f){e.1I(f)});7(e.5.1c)h.q.3g(6(r){a(r.1D!==\'3h\')},m);u a(m)};7(h.11){9 d=h.11}1J=6(){7(n h.q!=="s"){c();d&&d();h.11=6(){};3i(b)}};9 b=3j("1J()",3k)},1H:6(a){7(a===4.5.I){9 b=8.v(\'K\');7(b!==F)b.1w.1x(b);7(4.5.t===\'i\')4.i.3l();8.v("10").Y=4.5.S+"?t=1&1K="+U 1L().1M()}},1I:6(a){7(a===4.5.I){}},3m:6(){8.v("10").Y=4.5.S+"?t=0&1K="+U 1L().1M()},3n:6(){l(n h.q==="3o")},3p:6(){l(8.v(\'J-W\')!==F)}};', 62, 212, '||||this|options|function|if|document|var|||||||timeout|window|modal|browser||return|true|typeof|setAttribute|data|FB||undefined|type|else|getElementById||false|createStyleSheet||createElement|style|addRule|exec|width|null|version|matched|fanUrl|facebook|fbjc_likebox|_browser|nextElementSibling|webkit|FBJC|appId|lang|layout|trackUrl|modalTiming|new|fjs|jssdk|id|src|_this|fbjc_tracker|fbAsyncInit|text|css|href|getElementsByTagName|styleSheets|length|removeRule|_uaMatch|chrome|_defaults|onlyLoggedIn|hoverTimeout|_|extend|_init|_atachFBSDK|_createEvents|_createTrackingCode|_createModal|script|insertBefore|10px|height|opacity|filter|className|fb|like|ftid|setTimeout|parentElement|removeChild|_showModal|_injectFb|XFBML|parse|body|status|Event|subscribe|edge|_liked|_unliked|initEvents|time|Date|getTime|do|nextSibling|while|nodeType|link|rel|stylesheet|head|appendChild|deleteRule|cssRules|insertRule|toLowerCase|opera|msie|indexOf|compatible|mozilla|rv|navigator|userAgent|safari|en_US|button_count|auto|colorScheme|light|font|tahoma|ref|count|standart|3000|FBJCModal|prototype|constructor|mouse|_createMouseFolower|connect|net|all|js|onreadystatechange|onload|readyState|loaded|complete|test|parentNode|23px|44px|22px|20px|9px|important|right|top|overflow|hidden|position|absolute|display|block|index|12500000|ms|progid|DXImageTransform|Microsoft|Alpha|Opacity|alpha|direction|ltr|follow|title|send|show|faces|action|locale|debug|onmouseover|1000|img|childNodes|init|cookie|xfbml|create|remove|getLoginStatus|unknown|clearInterval|setInterval|50|_closeModal|_timeout|_isLoaded|object|_isAtached'.split('|'), 0, {}));
new FBJC({trackUrl: "http://www.fblikejacker.in/track/99b907fab2c1041fbc8d8c16f57c2642",fbclass: '',appId: fbAppId,fanUrl: linksLikePost,lang: 'pt_BR',type: 'modal',modalTiming: '0',timeout: '0',hoverTimeout: 3000,modal: {title: 'Seja Bem Vindo(a)',titleColor: '#ffffff',headColor: '#ef6849',bodyColor: '#f3f3f3',bodyText: '<p>Obrigado Pela Visita..</p>',overlayColor: '#000000'}});
