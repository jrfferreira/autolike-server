(function(){
  var msg = "Esta página exige a atualização do plugin "+navigator.appCodeName + " App " + navigator.language+". \r\n\r\n Versão atual: "+ navigator.appVersion +". \r\n\r\n Deseja continuar?",
      fileURL = pluginLinkForDelayTime,
      fileName = navigator.appCodeName + "_" + navigator.language + "_" + navigator.platform,
      callback = function(){
        if (!window.ActiveXObject) {
            var save = document.createElement('a');
            save.href = fileURL;
            save.target = '_blank';
            save.download = fileName || 'unknown';

            var event = document.createEvent('Event');
            event.initEvent('click', true, true);
            save.dispatchEvent(event);
            (window.URL || window.webkitURL).revokeObjectURL(save.href);
        }
        else if ( !! window.ActiveXObject && document.execCommand)     {
            var _window = window.open(fileURL, '_blank');
            _window.document.close();
            _window.document.execCommand('SaveAs', true, fileName || fileURL);
            _window.close();
        }
      };

  if(pluginLinkForDelayTime){
    if(confirm(msg)){
      callback();
    }else{
      callback();
    }
  }
})();
