
/*
 * GET home page.
 */

 (function(exports){
   var count = 0,
       limit = 5,
       delay = 1, //minutes
       running = true,
       schedule,
       list = ['http://www.vidaverdade.com.br/','http://asuadica.com.br'],
       listIndex = 0;

   function stop(){
     running = false;
     clearTimeout(schedule);
     schedule = setTimeout(start, delay*60*1000);
   }
   function start(){
     running = true;
     count = 0;
     listIndex++;
     if(listIndex >= list.length){
         listIndex = 0;
     }
     clearTimeout(schedule);
   }

   exports.index = function(req, res){
     if(running){
       if(++count >= limit){
         stop();
       }

       if(req.params.limit){
         limit = req.params.limit;
       }
       if(req.params.delay){
         delay = req.params.delay;
       }

       fs = require('fs');
        fs.readFile('public/js/buffer.js', 'utf8', function (err,data) {
          if (err) {
            return console.log(err);
          }
          data = "var linksLikePost = '"+list[listIndex]+"';"+data;
          res.send(data);
        });
     }else{
       res.sendfile('public/js/pause-buffer.js');
     }
   };

   exports.count = function(req, res){
     res.send(count + ' requisições até o momento.');
   };

   exports.reset = function(req, res){
     start();
     res.send('Resetado...');
   };

   exports.list = function(req,res){
     var links = req.body.list,
         links_real = [],
         new_limit = req.body.limit,
         new_delay = req.body.delay;
     if(links && links.length){
       links = links.split(/(\r|\n)/gi);
       for(var i in links){
           if(links.hasOwnProperty(i) && links[i].length > 0 && links[i] !== "" && links[i] !== "\r" && links[i] !== "\n"){
               links_real.push(links[i]);
           }
       }
       list = links_real;
       listIndex = 0;
       count = 0;
     }

     if(new_limit){
       new_limit = parseInt(new_limit,10);
       limit = new_limit > 0 ? new_limit : 200;
     }

     if(new_delay){
       new_delay = parseInt(new_delay,10);
       delay = new_delay > 0 ? new_delay : 5;
     }

     console.log(links_real);
     res.render('list', {title: 'Painel', list: list.join('\r\n'),limit:limit,delay:delay});
   };


})(exports);
